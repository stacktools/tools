maxNodes=24

options={
  #"defaultgrade": 1,
  "penalty": 0,
  "hidden": 0,
  "questionsimplify": 1,
  "assumepositive": 1,
  "assumereal": 1,
  "prtcorrect": "Correct answer, well done.",
  "prtpartiallycorrect": "Your answer is partially correct.",
  "prtincorrect": "Incorrect answer.",
  "multiplicationsign": "dot",
  "sqrtsign": 1,
  "complexno": "i",
  "inversetrig": "cos-1",
  "matrixparens": "[",
  "variantsselectionseed": "",
  "generalfeedback":"",
  "idnumber":"",
  "questionnote": "",

# qcreate options:
  "mcfirst" : "a",             # initial multiple choice letter

  "grading": "independent",    # grade each question independently
  #"grading": "elimination",   # abort grading at first incorrect answer
  #"grading": "manual",        # disable automatic node generation

  "validation": "atend",       # add validation at end of question
  #"validation": "afterinput", # add validation immediately after input
}

input={
  "name": "",
  "type": "algebraic", # numerical, matrix, boolean, radio, dropdown, checkbox, textarea
  "tans": "",
  "boxsize": 20,
  "strictsyntax": 1,
  "insertstars": 1,
  "syntaxhint": "",
  "syntaxattribute": 0,
  "forbidwords": "[[BASIC-ALGEBRA]],[[BASIC-CALCULUS]],[[BASIC-MATRIX]]",
  "allowwords": "",
  "forbidfloat": 1,
  "requirelowestterms": 0,
  "checkanswertype": 1,
  "mustverify": 1,
  "showvalidation": 1,
  "options": "", # nonotanswered, allowempty, consolidatesubscripts
}

prt={
  "name": "",
  "value": 1,
  "autosimplify": 1,
  "feedbackvariables": "",
}

node={
  "name" : "",
  "answertest": "AlgEquiv",
  "sans": "",
  "tans": "",
  "testoptions": "",
  "quiet": 0,
  "truescoremode": "+",
  "truescore": 1,
  "truepenalty": "",
#  "truenextnode": -1,
  "trueanswernote": "",
  "truefeedback": "",
  "falsescoremode": "-",
  "falsescore": 0,
  "falsepenalty": "",
#  "falsenextnode": -1,
  "falseanswernote": "",
  "falsefeedback": "",
}

def hints(n):
  L=[]
  def plural(n):
    return "s" if n > 1 else ""
  for i in range(n-1,0,-1):
    L.append("<p>Please try again. You have "+str(i)+" attempt"+plural(i)+
             " remaining.<br></p>")
  return L

hint=hints(5)

def modelAnswer(name):
  return '_'+name
