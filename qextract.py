#!/usr/bin/env python3

# qextract: Copyright (C) 2020-2023 John C. Bowman

import argparse
import xmltodict
import json
import re
import os

from stackDefaults import *

P=argparse.ArgumentParser(description='Extract Python dictionary from Moodle Stack XML questions.')
P.add_argument('question',nargs='+',help='Input question')
P.add_argument('-o',metavar='outdir',help='Output directory')
args=P.parse_args()

def unescape(s):
    return re.sub(r'\<[/]*span.*?\>','',re.sub(r'(?<!\\)\\t','    ',re.sub(r'(?<!\\)\\n',r'\n',re.sub(r'(?<!\\)"([^"]*)\\n(.*)(?<!\\)"','"""\g<1>\n\g<2>\n"""',re.sub(r'\<p.*?\>',r'\\n\\n',s.replace('<p>',r'\n').replace('</p>','').replace('<br>',r'\n')))))).replace('&nbsp;',' ').replace('&lt;','<').replace('&gt;','>').replace('&amp;','&').replace('\\\\','\\').replace('\\\\n','\\\\\n').replace('\\"','"')

def texify(s):
    return re.sub(r'^(.*".*)$','""\g<1>""',re.sub(r'\\[()]','$',re.sub('({@)|(@})','@',re.sub(r'\\\\',r'\\',s))))

def number(s):
    x=float(s)
    i=int(x)
    return i if i == x else x

def process(o,parent=None,parentKey=None):
    if isinstance(o,dict):
        for key in o:
            if o[key] == None:
                o[key]=''
        def prune(name):
            if name in o:
                del o[name]

        def pruneDefaults(name,default):
            if parentKey == name:
                for key in default:
                    if key in o:
                        if not isinstance(default[key],str):
                            o[key]=number(o[key])
                            Default=float(default[key])
                        else:
                            Default=default[key]
                        if o[key] == Default:
                            del o[key]

        prune('@format')
        prune('trueanswernote')
        prune('falseanswernote')
        prune('stackversion')

        for key,value in o.items():
            Key=None
            if isinstance(value,dict):
                for element in value:
                    if element == 'text':
                        Key=key
                        Value=value[element]
                        if Value == None:
                            Value=''
                        break
                if Key:
                    o[Key]=Value

        pruneDefaults(None,options)
        pruneDefaults('input',input)
        pruneDefaults('prt',prt)
        pruneDefaults('node',node)

        hints=True
        for key,value in o.items():
            if isinstance(value,list):
                List=[]
                for element in value:
                    if isinstance(element,dict):
                        for Key in element:
                            if Key == 'text':
                                Value=element[Key]
                                if Value == None:
                                    Value=''

                                List.append(Value)
                                break
                if len(List) > 0:
                    if key == 'hint' and List == hint:
                        hints=False
                    else:
                        o[key]=List

            process(value,o,key)

        if not hints:
            del o['hint']

    elif isinstance(o,list):
        for value in o:
            process(value,o,parentKey)
    elif isinstance(o,str):
        if isinstance(parent,list):
            for i in range(len(parent)):
                parent[i]=texify(parent[i])
        else:
            parent[parentKey]=texify(parent[parentKey])

for file in args.question:
    prefix=file.replace('.xml','')
    data=prefix+'.xml'
    prefix=os.path.basename(prefix)
    fin=open(data)
    D=xmltodict.parse(fin.read())['quiz']['question']
    d=''
    if isinstance(D,list):
        for q in D:
            if '@type' in q and q['@type'] == 'stack':
                d=q
    else:
        d=D

    if d == '':
        continue
    if '@type' in d:
        if d['@type'] != 'stack':
            continue

        del d['@type']

    process(d)

    filename=prefix+'.py'
    outdir=args.o
    if outdir != None:
        if outdir[len(outdir)-1] != '/':
            outdir += '/'

        filename=outdir+filename

    fout=open(filename,'w')
    print(filename)
    fout.write('options["grading"]="manual"\n\n')
    fout.write('question='+unescape(json.dumps(d,indent=4)).replace(r'\\n',r'\n'))
