# Skeleton template for creating Stack questions.
# Terminate each definition with a comma. Enclose multiline strings with """.
# See stackDefaults.py for customizations.

# The argument to the function hints specifies the number of tries;
# if this line is removed the default in stackDefaults will be used.
hint=hints(5)

question={
  "questiontext": """
If $n=@n@$ then $n^2$ = [[input:nsquared]] and $n^3$ = [[input:ncubed]].
""",

  "questionvariables": """
n:rand_with_step(-10,10,1);
_nsquared:n^2;
_ncubed:n^3;
""",

  "questionnote": "Variables: @n@",

  "generalfeedback": """
$n^2=n*n=@n*n@$ and $n^3=n*n*n=@n*n*n@$.
""",

  "input": [
    {
      "name": "nsquared",
      "boxsize": 5,
    },
    {
      "name": "ncubed",
      "boxsize": 5,
    },
  ],

  "multiplicationsign": "none",

  "defaultgrade" : 10,

  "prt": [
    {
      "feedbackvariables": """
""",
      "node": [
        {
          "sans": "nsquared",
          "tans": "_nsquared",
        },
        {
          "sans": "ncubed",
          "tans": "_ncubed",
        },
      ]
    },
  ],

  "tags": {
    "tag": ["easy","template"],
  },
}
