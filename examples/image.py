hint=hints(1)
options["mcfirst"]=""

def simplexDirection(label,a):
  label=label+a
  return "simplex direction "+a+", which is in the direction of [[input:dir"+label+"]] is [[input:feasible"+label+"]]"

def simplexDirections(label,a,b):
  label=str(label)
  a=str(a)
  b=str(b)
  return "$\bullet$ if we choose the nonbasic variables to be $x_"+a+", x_"+b+"$, then "+simplexDirection(label,a)+", while "+simplexDirection(label,b)+"."+nl+nl+"[[feedback:"+label+"]]"+nl+nl+"<p><br></p>"

def inputDirection(L,label,a):
  label=label+a
  L.append(
    {
      "name": "dir"+label,
      "type": "radio",
    }
  )
  L.append(
    {
      "name": "feasible"+label,
      "type": "dropdown",
    }
  )

def inputDirections(L,label,a,b):
  a=str(a)
  b=str(b)
  inputDirection(L,label,a)
  inputDirection(L,label,b)

def inputs():
  L=[]
  L.append(
    {
      "name": "basic",
      "type": "checkbox",
    }
  )
  L.append(
    {
      "name": "feasible",
      "type": "checkbox",
    }
  )
  L.append(
    {
      "name": "degenerate",
      "type": "checkbox",
    }
  )
  inputDirections(L,"a",1,2)
  inputDirections(L,"b",1,3)
  inputDirections(L,"c",2,3)
  return L

def nodes(label,a):
  return [
    {
      "input": "dir"+label+a,
      "name": "dir"+a,
      "sans": "check(dir"+label+a+",_dir"+label+a+")",
      "tans": "true",
    },
    {
      "input": "feasible"+label+a,
      "name": "feasible"+a,
      "sans": "check(feasible"+label+a+",_feasible"+label+a+")",
      "tans": "true",
    }
  ]

def prtPair(label,a,b):
  a=str(a)
  b=str(b)
  return {
    "name": label,
    "node": nodes(label,a)+nodes(label,b),
  }

question={
  "questiontext": tex("macros.tex")+"""
A linear programming problem in standard form has $n=4$
decision variables $x_1,x_2,x_3,x_4$ and $m=2$ equality constraints.
The shaded region in the figure represents the projection of the
feasible set onto the two-dimensional plane defined by the equality
constraints $\vA\vx = \vb$.

[[image:degeneracy.svg]]

Identify which of the points $A$, $B$, $C$, $D$ are

Basic: [[input:basic]]

Feasible: [[input:feasible]]

Degenerate: [[input:degenerate]]

At the point $B$:

"""+simplexDirections("a",1,2)+simplexDirections("b",1,3)+simplexDirections("c",2,3),

  "questionvariables": """
_basic:mcq(["$A$",true],["$B$",true],["$C$",true],["$D$",true]);
_feasible:mcq(["$A$",false],["$B$",true],["$C$",true],["$D$",true]);
_degenerate:mcq(["$A$",false],["$B$",true],["$C$",false],["$D$",false]);

_dira1:mcq(["$\vf$",false],["$\vg$",true],["$\vh$",false],["$-\vf$",false],["$-\vg$",false],["$-\vh$",false]);
_feasiblea1:[["feasible",true],["infeasible",false]];
_dira2:mcq(["$\vf$",true],["$\vg$",false],["$\vh$",false],["$-\vf$",false],["$-\vg$",false],["$-\vh$",false]);
_feasiblea2:[["feasible",true],["infeasible",false]];

_dirb1:mcq(["$\vf$",false],["$\vg$",false],["$\vh$",true],["$-\vf$",false],["$-\vg$",false],["$-\vh$",false]);
_feasibleb1:[["feasible",false],["infeasible",true]];
_dirb3:mcq(["$\vf$",true],["$\vg$",false],["$\vh$",false],["$-\vf$",false],["$-\vg$",false],["$-\vh$",false]);
_feasibleb3:[["feasible",true],["infeasible",false]];

_dirc2:mcq(["$\vf$",false],["$\vg$",false],["$\vh$",false],["$-\vf$",false],["$-\vg$",false],["$-\vh$",true]);
_feasiblec2:[["feasible",false],["infeasible",true]];
_dirc3:mcq(["$\vf$",false],["$\vg$",true],["$\vh$",false],["$-\vf$",false],["$-\vg$",false],["$-\vh$",false]);
_feasiblec3:[["feasible",true],["infeasible",false]];
""",

  "generalfeedback": """
""",

  "input": inputs(),

  "prt": [prtPair("a",1,2),prtPair("b",1,3),prtPair("c",2,3)],

  "tags": {
    "tag": ["easy","simplexDirections"],
  },
}
