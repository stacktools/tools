options["grading"]="elimination"

question={
  "questiontext": """
To evaluate the definite integral $$\int_@a@^@b@ @f*g@\, dx$$
one first computes the indefinite integral $\int @f*g@\, dx$ by parts:

[[input:boundary]] $-\int$ [[input:integrand]]$\,dx$,
to obtain the indefinite integral [[input:antiderivative]].

The definite integral then evaluates to [[input:answer]].
""",

  "questionvariables": """
a:0;
b:pi;
n:rand_with_step(3,11,2);
f:x;
g:cos(n*x);

F(x):=integrate(f*g,x);
G(x):=integrate(g,x);
_boundary:f*G(x);
_integrand:diff(f,x)*G(x);
_antiderivative:F(x)+C;
_answer:at(F(x),x=b)-at(F(x),x=a);
""",

  "questionnote": "Variables: @n@",

  "input": [
    {
      "name": "boundary",
      "boxsize": 8,
    },
    {
      "name": "integrand",
      "boxsize": 8,
    },
  ],

  "multiplicationsign": "none",

  "prt": [
    {
      "feedbackvariables": """
Int(ans,Ans,x):=diff(strip_int_const(ans,x)-strip_int_const(Ans,x),x);
""",
      "node": [
        {
          "name": "antiderivative",
          "sans": "Int(antiderivative,_antiderivative,x)",
          "tans": "0",
        },
      ]
    },
  ],

  "tags": {
    "tag": ["easy","integration by parts","integration","definite integrals"]
  },
}
