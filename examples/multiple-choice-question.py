hint=hints(1);

question={
  "questiontext": """
An example of a transcendental function is [[input:function]]
""",

  "questionvariables": """
_function:mcq(shuffle(
["the identity function $f(x)=x$",false],
["the exponential function $f(x)=e^x$",true],
["the natural logarithm $f(x)=\log x$",true]),
["all of the above",false],
["none of the above",false]
);
""",

  "input": [
    {
      "name": "function",
      "type": "radio",
    },
  ],

  "tags": {
    "tag": ["mc","transcendental"],
  },
}
