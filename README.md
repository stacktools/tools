# Stack Tools: Copyright (C) 2020-23 John C. Bowman

Tools for creation and curation of Stack Moodle questions:

* ```qextract``` extracts a Python dictionary from a Moodle XML question;

* ```qcreate``` converts a Python dictionary to a Moodle XML question;

* ```qjoin``` consolidates questions into a single XML question library;

* ```qsplit``` assigns IDs (and tags) of new questions based on the author
and initial date of import into the git repository (and categories).

## Installation

You need ```python3``` with these additional libraries:

```
pip3 install --user python-slugify lxml xmltodict

```

## Extraction
Usage:

```
qextract [-h] [-o outdir] question [question ...]
```

For each given XML input, a Python dictionary will be written to a .py file,
which can be edited much more easily than using the Moodle question editor.
It can then be converted back to an XML file with ```qcreate``` and imported
into Moodle.


## Creation

Usage:

```
qcreate [-h] [-u ccid] [-o outdir] [-t] [-m] [-s] [-v] source [source ...]
```

Here ```ccid``` is an optional author ID (by default, the environment
variable ```USER```) that will be prefixed to the current time
(year-month-day-hour-minute) to fill in the ```idnumber``` field of new
questions. Question data is entered via a dictionary in a Python template
```source```. Sample templates are provided in the ```examples```
directory. The options ```-t```, ```-m```, ```-s```, and ```-v``` support local testing of LaTeX and Maxima code, with optional solutions typeset into a PDF file.

Creating a question involves three steps:

1. Edit ```examples/template.py``` and fill in the fields as needed.
   Or begin with a Python dictionary extracted from an existing XML question
   using ```qextract```.

Include ```[[input::name]]``` in the ```questiontext``` for each student input,
where name represents a unique valid variable name. We recommend using
meaningful variable names as they will appear in the ```specificfeedback```.
Corresponding ```validation```, ```input```, and skeleton ```prt``` fields are
added automatically. The model (teacher) answer, which by default is the
input variable prefixed by an underscore, can be defined in the
```questionvariables```. The default values in ```stackDefaults.py``` can be
adjusted or locally overridden in each template.
Fully worked solutions can be entered in the ```generalfeedback``` field.
TeX macros in a file ```macros.tex``` can be included by changing
```
  "questiontext": """
```
to
```
  "questiontext": tex("macros.tex")+"""
```
Similarly, Maxima code in a file ```common.mac``` can be loaded by prepending
```load("common.mac")``` to the appropriate field, and stack code in a file
```common.txt``` can be included by prepending ```stack("common.txt")``` to the
desired field. Python functions can also be used to generate the field entries.
To import a module ```common.py``, use ```exec(python("common.py"))```.

The ```questionvariable``` and ```feedbackvariables``` fields accept
semicolons and newlines to allow Maxima code to be tested before deployment
(consistently enter semicolons after every statement OR no semicolons at all).
The special ```[[image:filename]]``` construct can be used to embed an image
from ```filename``` into the question text or general feedback.

2. Create a Moodle XML file:
```
cd examples
../qcreate.py template.py
```
Any Python errors will be caught in this step. To also test the LaTeX code,
specify the ```-t``` option. If you want to test the Maxima code, install Maxima 5.43, include the ```-m``` option and put a copy of the stack git repo in a directory ```mac``` in the current directory (or set the environment variable ```MAXIMA_USERDIR``` to point to the stack repo):
```
mkdir mac
cd mac
git clone https://github.com/maths/moodle-qtype_stack stack
```
Once you have the ```-m``` option working, you can add solutions to the LaTeX file with the ```-s``` option.

3. Import the generated XML file into Moodle and preview the question.

If the question shows up as blank or there are errors, edit the question
and then select ```Save changes``` to display the errors.

## Curation

Usage:

```
usage: qsplit [-h] -u ccid library
qjoin [-h] -o library question [question ...]
```

To export a category from Moodle to Git and back again,
first export ```TOPIC.xml``` from Moodle with categories but WITHOUT context.
Then do the following in the top-level Git directory:

```
mkdir TOPIC
../tools/qsplit.py -u ccid /pathTo/TOPIC.xml
git add *.xml
git commit *.xml
git push

git pull
../tools/qjoin.py -o /pathTo/out.xml *.xml
```

We recommend keeping the output file in a different directory than
the input files. You can then import the entire ```out.xml``` library
or individual ```*.xml``` files directly into Moodle.
